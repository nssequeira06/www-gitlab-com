# This test response doesn't include the "fields" attribute.
TEST_RESPONSE = {
  "title": "GitLab Team",
  "employees": [
    {
      "id": "1",
      "status": "Active",
      "employeeNumber": "101",
      "preferredName": nil,
      "firstName": "Chris",
      "lastName": "GitLab",
      "department": "Product",
      "jobTitle": "CEO",
      "hireDate": "2015-01-01",
      "workEmail": "chris@gitlab.com",
      "country": "United States",
      "supervisorEId": nil
    },
    {
      "id": "2",
      "status": "Inactive",
      "employeeNumber": "102",
      "preferredName": nil,
      "firstName": "Kim",
      "lastName": "Smith",
      "department": "Development",
      "jobTitle": "Engineering Manager",
      "hireDate": "2016-01-01",
      "workEmail": "kim@gitlab.com",
      "country": "United States",
      "supervisorEId": "1"
    },
    {
      "id": "3",
      "status": "Active",
      "employeeNumber": "103",
      "preferredName": "Jay",
      "firstName": "Jeri",
      "lastName": "Anderson",
      "department": "Development",
      "jobTitle": "Backend Engineer",
      "hireDate": "2017-01-01",
      "workEmail": "jay@gitlab.com",
      "country": "Netherlands",
      "supervisorEId": "2"
    },
    {
      "id": "4",
      "status": "Active",
      "employeeNumber": "104",
      "preferredName": nil,
      "firstName": "Alex",
      "lastName": "Vue",
      "department": "Development",
      "jobTitle": "Frontend Engineer",
      "hireDate": "2018-01-01",
      "workEmail": "alex@gitlab.com",
      "country": "Netherlands",
      "supervisorEId": "1"
    },
    {
      "id": "5",
      "status": "Active",
      "employeeNumber": "999",
      "preferredName": nil,
      "firstName": "Fran",
      "lastName": "Future",
      "department": "Research",
      "jobTitle": "Time Traveller",
      "hireDate": "2050-01-01",
      "workEmail": "fran@gitlab.com",
      "country": nil,
      "supervisorEId": "1"
    }
  ]
}.freeze

describe BambooHR::Client do
  before do
    stub_request(:post, format(BambooHR::API_URL, resource: 'reports/custom'))
      .to_return(body: JSON.generate(TEST_RESPONSE))
  end

  describe '#employees' do
    context 'when requesting only active and current employees' do
      let(:client) { described_class.new('test_token') }
      let(:employees) { client.employees }

      it 'returns the correct amount of entries' do
        expect(employees.length).to eq 3
      end

      it 'omits references to inactive employees' do
        expect(employees[1].manager_id).to eq nil
      end

      it 'uses preferred names' do
        expect(employees[1].name).to eq 'Jay Anderson'
      end

      it 'converts employee numbers' do
        expect(employees[1].bamboohr_id).to eq 103
      end

      it 'converts start dates' do
        expect(employees[2].start_date).to eq Date.new(2018, 1, 1)
      end

      it 'maps managers' do
        expect(employees[2].manager_id).to eq 101
      end
    end

    context 'when requesting all employees' do
      let(:client) { described_class.new('test_token') }
      let(:employees) { client.employees(inactive: true, future: true) }

      it 'returns the correct amount of entries' do
        expect(employees.length).to eq 5
      end

      it 'references inactive employees' do
        expect(employees[2].manager_id).to eq 102
      end
    end
  end
end
