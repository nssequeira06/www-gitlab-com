---
layout: markdown_page
title: "Product Section Direction - Dev"
description: "The Dev Section is made up of the Manage, Plan, and Create stages of the DevOps lifecycle and encompasses a number of analyst categories"
canonical_path: "/direction/dev/"
---

- TOC
{:toc}

Last Reviewed: 2021-01-12

<%= devops_diagram(["Manage","Plan","Create"]) %>

## Dev Section Overview

The Dev Section is made up of the [Manage](/handbook/product/categories/#manage-stage), [Plan](/handbook/product/categories/#plan-stage), and [Create](/handbook/product/categories/#create-stage) stages of the DevOps lifecycle. The scope for the Dev section is wide and encompasses a number of analyst categories including Value Stream Management (VSM), Project Portfolio Management (PPM), Enterprise Agile Planning Tools (EAP), Source Code Management (SCM), Integrated Development Environments (IDEs), Design Management, and even Information Technology Service Management (ITSM). It is difficult to truly estimate Total Available Market (TAM) for the Dev Section, as our scope includes so many components from various industries, but market research from the analyst firm [IDC](https://www.idc.com/about) indicates the estimated [TAM](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=1474156035) in 2020 is roughly ~$2B, growing to ~$4.3B in 2024 (18.31% CAGR). Analysis: [Manage](https://docs.google.com/spreadsheets/d/1357Zwbf0yTjcFBuCdX2HnNLBaF9uWnW7Auv4F94aoSo/edit#gid=0) [Plan](https://docs.google.com/spreadsheets/d/1dDNSR_mE4peeOc_3Xhqm_VLoTyk4gSbcbiGfuMrOg6g/edit#gid=0) [Create](https://docs.google.com/spreadsheets/d/1KSwTVPIvMO8IXkMqA40Ms1rL-glmo-w7MjZvUrjMFGA/edit#gid=0). NOTE: The large disparity in the TAM/SAM (Serviceable Available Market) approaches is due to percentage attribution of DevOps revenue. For example, in the IDC report, it specified a certain percentage of Atlassian revenue for DevOps, but in the bottoms up analysis most of Atlassian revenue would have been counted since we looked at all project/portfolio management revenue.

Nearly [half of organizations](https://drive.google.com/file/d/17ZSI2hGg3RK168KHktFOiyyzRA93uMbd/view?usp=sharing) still have not adopted DevOps methodologies, despite [data](https://drive.google.com/file/d/17MNecg84AepxWlSDB5HjNBrCJggaS9tP/view?usp=sharing) that indicates far higher revenue growth for organizations that do so. Migrating a code base to a modern, Git-backed source control platform like GitLab can often be the first step in a DevOps transformation. As such, we must provide industry-leading solutions in source code and code review, as this is not only the entry into DevOps for our customers, but typically the entry into the GitLab platform. Once a user has begun utilizing repositories and code review features like Merge Requests, they often shift “left” and “right” to explore and utilize other capabilities in GitLab, such as CI and project management features.

Per our [Stage Monthly Active User data](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/) we know that Create and Plan have the highest usage amongst GitLab stages. As such, these stages must focus on security fixes, bug fixes, performance improvements, UX improvements, and depth more than other areas of GitLab.

### Dev Section Accomplishment, News, and Updates

#### Section News & Team Member Updates
* [David DeSanto's responsibilities have expanded to include leading the Dev and Sec Sections](https://gitlab.slack.com/archives/CL55Q4U0K/p1612561609472800)
* [We are actively hiring Group Manager, Product Management roles for the Create, Manage, and Plan stages](https://gitlab.com/gitlab-com/Product/-/issues/2127)

#### Important PI Highlights
1. [Dev Section exceeded TMAU target of 2.73M by 1.4% by end of FY21](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#dev---section-tmau---estimated--recorded-sum-of-dev-section-smau)
1. [Manage:Access experienced 10% growth MoM during the holiday season](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#manageaccess---paid-gmau---mau-using-paid-saml)
1. [Plan:Project Management exceeded its quarterly metrics target in January!](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#plan-planproject-management---smau-paid-gmau---mau-interacting-with-issues)
1. [Plan:Product Planning exceeded its quarterly metrics target in January!](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#planproduct-planning---paid-gmau---mau-creating-epics-each-month)
1. [Interacting with Iterations MAU increased by 52% MoM!](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#plan-planproject-management---smau-paid-gmau---mau-interacting-with-issues)
1. [Create:Ecosystem Total Active Project Integrations passed 5M!](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#createecosystem---other---total-active-project-integrations)

#### Recent Accomplishments
1. [Send an email to an issue](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#send-an-email-to-an-issue)
1. [Click and drag multiline merge request comments](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#click-and-drag-multiline-merge-request-comments)
1. [Rebase quick action for merge requests](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#rebase-quick-action-for-merge-requests)
1. [Distributed Reads for Gitaly Cluster](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#distributed-reads-for-gitaly-cluster)
1. [Scope a board to the current iteration](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#scope-a-board-to-the-current-iteration)
1. [Use rich output for Jupyter notebooks](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#use-rich-output-for-jupyter-notebooks)
1. [Migrate Groups directly between instances](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#migrate-groups-directly-between-instances)
1. [Send build information through Jira connect app](https://gitlab.com/gitlab-org/gitlab/-/issues/14178)
1. [POC for Settings cascading](https://gitlab.com/gitlab-org/gitlab/-/issues/291082)
1. [Welcome email for SAML and SCIM provisioned accounts](https://about.gitlab.com/releases/2021/01/22/gitlab-13-8-released/#welcome-email-for-saml-and-scim-provisioned-accounts)

#### What's Ahead

1. [Group-level merge request approval rules](https://gitlab.com/groups/gitlab-org/-/epics/4367)
1. [Create project-level policies framework for deny (Policies MVC)](https://gitlab.com/groups/gitlab-org/-/epics/4168)
1. [SAML Group Sync](https://gitlab.com/gitlab-org/gitlab/-/issues/118)
1. [Gitaly Variable Replication Factor](https://gitlab.com/groups/gitlab-org/-/epics/3372)
1. [Simplify group and projects](https://gitlab.com/groups/gitlab-org/-/epics/2885)
1. [Epic Boards](https://gitlab.com/groups/gitlab-org/-/epics/4117)
1. [Horizontal navigation for value stream analytics](https://gitlab.com/gitlab-org/gitlab/-/issues/246493)
1. [Add toggles for left sidebar items](https://gitlab.com/groups/gitlab-org/-/epics/3719)
1. [Required Jobs at the Group Level](https://gitlab.com/groups/gitlab-org/-/epics/3156)


## Dev Section FY22 Themes

The primary themes for the Product department in FY22 are:

1. Become a leader in Application Security Testing
1. Adoption through usability
1. SaaS first

The Dev Section will primarily focus on adoption through usability and SaaS first, but will also contribute to becoming a leader in AST through connecting developer workflows through issues and merge requests to AST.

#### Adoption through usability

The Dev Section has the highest TMAU of all sections, over [2.7M TMAU](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#dev---section-tmau---estimated--recorded-sum-of-dev-section-smau). Because of this, the Dev section will be highly focused on creating a lovable experience for several core worklows throughout the product. We'll also be focused on product architecture improvements, like simplifying groups/projects, which will enable efficiency gains of GitLab product teams as well as an easier to understand user experience for new users of GitLab. Here are the jobs to be done the Dev section will focus on to improve adoption through usability:

1. Easily understand how to set up and manage my organization and projects in GitLab - solved for via [groups and projects moving to namespaces](https://gitlab.com/groups/gitlab-org/-/epics/2885)
1. Easily find exactly what I'm looking for in GitLab - solved for via [improving settings and navigation](https://gitlab.com/groups/gitlab-org/-/epics/4831)
1. Provide an out of the box experience that requires little to no configuration - solved for via enabling all possible Dev section features to be [working by default](https://about.gitlab.com/handbook/product/product-principles/#configuration-principles)
1. Ensure I'm always looking at the most up to date information - solved for by focusing on realtime experiences
1. Conduct an efficient code review - solved for via code review usability improvements and MR reviewers
1. Easily manage my portfolio - solved for by epic boards, roadmap improvements, and workflow automation

#### SaaS first

The Dev section will be highly focused on providing a SaaS first experience for GitLab. For other sections, this may mean deciding to ship features at the group or project level instead of instance, but for us, it's about making fundamental product changes to provide an instance like experience at the group level. We'll do this by shipping a workspace, which is a new instance level object, moving applicable Admin Panel settings and features into the workspace, and ensuring proper cascading of these settings and features. Here are the initiatives the Dev section will focus on to ensure a SaaS first experience:

1. Deliver workspaces and settings/feature cascading
1. Provide access to every Dev feature to .com users
1. Deliver provisioned accounts
1. Deliver variable replication factor settings and auto rebalancing for Gitaly Cluster

#### Stage focus

Within each stage, the listed items below are in **order of priority**. The top priority for each stage or group is:

* Manage: [Workspaces to replace instance-level functionality](https://gitlab.com/groups/gitlab-org/-/epics/4257)
* Plan: [Groups and Projects Simplification](https://gitlab.com/groups/gitlab-org/-/epics/2885)
* Create: [Gitaly Cluster](https://gitlab.com/groups/gitlab-org/-/epics/1489)
* Ecosystem: [Atlassian Integration](https://gitlab.com/gitlab-org/gitlab/-/boards/1789772?label_name[]=atlassian)

## Who is the Dev section targeting?
We identify the [personas](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) the Dev section features are built for. In order to be transparent about personas we support today and personas we aim to support in the future we use the following categorization of personas listed in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Dev section has features that make it useful to the following personas today.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Parker - Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟨 [Presley - Product Designer](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟨 [Cameron - Compliance Manager](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. 🟨 [Rachel - Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Simone - Software Engineer in Test](/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Allison - Application Ops](/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. 🟨 [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Sidney - Systems Administrator](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

### Medium Term (1-2 years)
As we execute our [3-year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single application that enables collaboration between cloud native development and platform teams.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/strategic-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Delaney - Development Team Lead](/handbook/marketing/strategic-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Parker - Product Manager](/handbook/marketing/strategic-marketing/roles-personas/#parker-product-manager)
1. 🟩 [Presley - Product Designer](/handbook/marketing/strategic-marketing/roles-personas/#presley-product-designer)
1. 🟩 [Cameron - Compliance Manager](/handbook/marketing/strategic-marketing/roles-personas/#cameron-compliance-manager)
1. 🟩 [Simone - Software Engineer in Test](/handbook/marketing/strategic-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Rachel - Release Manager](/handbook/marketing/strategic-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Allison - Application Ops](/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
1. 🟨 [Priyanka - Platform Engineer](/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Sidney - Systems Administrator](/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

## 3 Year Strategy

In three years, the Dev Section market will:

* Centralize around Git as the version control of choice for design assets, gaming, silicon designs, and AI/ML models.
* Have a market leader emerge in the value stream management space. Currently, the market is fragmented with most players focused on integrations into various DevOps tools.
* Adopt a mindset shift from project management to product management.
* Recognize the value of a single platform for all software creation activities, including product management.
* See an uptick in startups, applications, and internal business logic being built on the backs of low or no-code platforms. We recently merged in a low/no-code [direction](/direction/create/nolowcode) page if you'd like more information on our direction in this space.
* Related to low/no code frameworks mentioned above, the market will see an increase in apps built on AI technology. In June of 2020, [GPT-3](https://en.wikipedia.org/wiki/GPT-3) was released and early adopters shared incredible use cases based on this technology. Within 3 years, it's likely this technology has advanced significantly and is starting to disrupt modern software development.
* Coalesce around the notion of a single platform for DevOps, also known as Value Stream Delivery Platforms.

As a result, in three years, GitLab will:

* Provide a next-generation, highly performant Git-backed version control system for large assets, such as ML models. Our goal in three years should be to host the most repositories of these non-code assets.
* Emerge as the leader in VSM and be recognized in the industry by customers and analysts as such. Our goal in three years should be to provide the best insights into the product development process that no other tool can come close to, as we have a [unified data model](https://www.ca.com/en/blog-itom/what-is-a-unified-data-model-and-why-would-you-use-it.html) due to GitLab being a single platform.
* Develop an industry-leading product management platform where multiple features and products can be measured and managed easily.
* Research and potentially add capabilities for "no code" workflows and GPT-3 developed applications.
* Ensure we are the best VSDP on the market.

## 3 Year Themes

Our direction for the Dev section is to provide the world’s best product creation and management platform.
We believe we have a massive opportunity to change how cross-functional, multi-level teams collaborate by
providng a solution that breaks down organizational silos and enables maximum value delivery.
We want to provide a solution that enables higher-quality products to be quickly iterated upon. We also want to make it effortless for companies to migrate to GitLab. In order to obtain adoption from incumbent tools, GitLab has to provide *substantially more value*
than our competitors, but do so by maintaining simplicity. The following themes listed below represent how we believe we will deliver this value
and is our view of what will be important to the market and to GitLab over the next 3 to 5 years. As such,
they will be the cornerstone of our 3-year strategy, and all activities in the 1-year plan should advance
GitLab in one or more of these areas.

<%= partial("direction/dev/themes/automated_code_review") %>

<%= partial("direction/dev/themes/value_stream_measurement") %>

<%= partial("direction/dev/themes/more_devops_personas") %>

<%= partial("direction/dev/themes/enterprise_digital_transformation") %>

<%= partial("direction/dev/themes/project_to_product") %>

<%= partial("direction/dev/themes/remote_development") %>

## 1 Year Plan: What’s Next for Dev

Please see the [categories page](/handbook/product/categories/#dev-section) for a more detailed look at Dev's plan by exploring `Direction` links in areas of interest. This page will highight direction themes for both one year and three year timelines.

<%= partial("direction/dev/plans/manage") %>

<%= partial("direction/dev/plans/plan") %>

### Create

<%= partial("direction/dev/plans/create") %>

### Themes that cross all Dev stages
**Performance and availability:** We must invest in the performance, stability, and availability of our application. We will do this by focusing on [application limits](https://gitlab.com/groups/gitlab-org/-/epics/1737#note_202179305), [diff load times](https://gitlab.com/groups/gitlab-org/-/epics/1816), and ensuring [availability](https://about.gitlab.com/handbook/product/product-processes/#how-we-prioritize-work) is top of mind.

Growth driver: Retention

### What we're not currently focusing on

Choosing to invest in the above areas in 2020 means we will choose not to:
* Efficiency recommendations: Provide recommendations where customers can improve their efficiency in the DevOps lifecycle. This will likely require comparisons amongst many GitLab users and an AI engine to make intelligent recommendations. We won't be working on features that help companies answer, “Am I doing the right activities?” These improvements will come in years two and three of the VSM plan.
* Container based IDEs or remove dev environments. We recently merged in a community contribution from GitPod, which provides this functionality and will understand the usage of this feature before making large investments here.
* Feature objects: Creating first class feature objects per the project management morphs into product management block below. While we think this is important, we must spend this year building a better foundation in portfolio management by making our groups/projects structure more simple and delivering epic boards.
* Wikis: Investments in wikis such as WYSIWYG editing or real time collaboration ala Notion.

## Stages & Categories

<%= partial("direction/dev/strategies/manage") %>

<%= partial("direction/dev/strategies/plan") %>

<%= partial("direction/dev/strategies/create") %>

## What's Next 

<%= direction["all"]["all"] %>
