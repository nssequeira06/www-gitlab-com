---
layout: markdown_page
title: "The Ally Lab"
description: Learn what is an ally, how to be and ally and what it means to be an ally.
canonical_path: "/company/culture/inclusion/being-an-ally/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What is an ally? 

A diversity, inclusion and belonging "ally" is someone who is willing to take action in support of another person, in order to remove barriers that impede that person from contributing their skills and talents in the workplace or community.

Being an ally is a verb, this means that you proactively and purposefully take action and is not something forced upon you. 

## How to be an ally 

It is not required to be an ally to work at GitLab. At GitLab it is required to [be inclusive](/handbook/values/#diversity-inclusion). 

Being an ally goes a step beyond being inclusive to taking action to support marginalized groups. The first step in being an ally is self-educating. 

This ally lab with provide you with some of the tools, resources and learning activities to help you grow as an ally.

## Skills and Behaviours of allies 

To be an effective ally it is important to understand some of the skills and behaviours great allies exhibit. 

* TBC
* TBC
* TBC
* TBC
* TBC

## What it means to be an ally
* Take on the struggle as your own
* Stand up, even when you feel uncomfortable
* Transfer the benefits of your privilege to those who lack it
* Acknowledge that while you, too, feel pain, the conversation is not about you

## Concepts & Terms

1. Privilege: an unearned advantage given to some people but not all 
1. Oppression: systemic inequality present throughout society that benefits people with more privilege and is a disadvantage to those with fewer privileges 
1. Ally: a member of a social group that has some privilege, that is working to end oppression and understands their own privilege 
1. Power: The ability to control circumstances or access to resources and/or privileges  
1. Marginalized groups:  a person or group that are treated as unimportant, insignificant or of lower status. In a workplace setting, employees could be treated as invisible, as if they aren't there or their skills or talents are unwelcome or unnecessary
1. Performative allyship: referring to allyship that is done to increase a persons social capital rather than because of persons devotion to a cause. For example some people used #metoo during the Me Too movement, without actually bringing more awareness or trying to effect change. 

## Tips on being an ally

1. Identifying your power and privilege helps you act as an ally more effectively
1. Follow and support those as much as possible from marginalized groups 
1. When you make a mistake, apologize, correct yourself, and move on
1. Allies spend time educating themselves and use the knowledge to help
1. Allies take the time to think and analyze the situation
1. Allies try to understand Perception vs. Reality
1. Allies don’t stop with their power they also leverage others powers of authority

See our [Ally Resources Page](https://about.gitlab.com/handbook/communication/ally-resources/) for more resources on being an ally. 

## Ally Training 

We held a 50 minute [Live Learning](https://about.gitlab.com/handbook/people-group/learning-and-development/#live-learning) Ally Training on 2020-01-28. The recording follows along with the [slide deck](https://docs.google.com/presentation/d/18Qyn2mBJu0Loq3x_RT5bL2lnL-3YHvac1sQhmqqZNso/edit?usp=sharing) and [agenda](https://docs.google.com/document/d/1lGPImuahahjDejI5-9cNNCg-NMQJ4GCHO6n0fcntjs8/edit?usp=sharing). 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/wwZeFjDc4zE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRRWRr6Aclf-Ddft1MKjtwdQ98216WEGG7LWJ2UZ3MOSnRZ0-cMhaPNeHpjQ4kk4c7pKkpuV-nvDN-2/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

## Being an Ally Knowledge Assessment

Anyone can become certified in GitLab Ally Training. To obtain certification, you will need to complete this [Being an Ally knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLScmd2Z9Vz66yYR6kCKZBZM61aDMFPHlViWCr2ZsGqFbbxIa4w/viewform) and earn at least an 80%. Once the quiz has been passed, you will receive an email with your certification that you can share on your personal LinkedIn or Twitter pages. If you have questions, please reach out to our L&D team at `learning@gitlab.com`.

## External Ally Training 

There are some essential skills that are required to be an ally, here are a number of trainings that will help you enhance your allyship skills. Some of these are not allyship specific but will sharpen your skills in those important areas. 

[Equality Ally Strategies](https://trailhead.salesforce.com/en/content/learn/modules/workplace_equality_ally_strategies)
[Champion Workplace Equality](https://trailhead.salesforce.com/en/content/learn/trails/champion_workplace_equality)
[Effective Listening](https://www.linkedin.com/learning/effective-listening/improve-your-listening-skills?u=2255073)
[How to engage meaningfully in Allyship](https://www.linkedin.com/learning/paths/how-to-engage-meaningfully-in-allyship-and-anti-racism?u=2255073)
[Becoming a true ally](https://www.linkedin.com/learning/inclusive-mindset-for-committed-allies/becoming-a-true-ally?u=2255073)
[Building Trust](https://www.linkedin.com/learning/building-trust-6/building-trust?u=2255073)
[Why trust matters](https://www.linkedin.com/learning/why-trust-matters-with-rachel-botsman/the-importance-of-trust?u=2255073)

## Ally Learning Activity and Scenarios

## GitLab Diversity, Inclusion & Belonging resources

Allies familiarize themselves with GitLab's general DIB content

- [Diversity, Inclusion & Belonging page](/company/culture/inclusion/)
- [Gender and Sexual Orientation Identity Definitions and FAQ](/handbook/people-group/gender-pronouns/)
- [DIB training resources](https://about.gitlab.com/company/culture/inclusion/#diversity-inclusion--belonging-training-and-learning-opportunities)
- [Unconscious bias](/handbook/communication/unconscious-bias/)

## Ally Resources

Here are additional resources on being an ally

- [Guide to allyship](https://www.guidetoallyship.com)
- [5 Tips For Being An Ally](https://www.youtube.com/watch?v=_dg86g-QlM0)
- [Ally skills workshop](https://frameshiftconsulting.com/ally-skills-workshop/). Check out the materials section with [a handout PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20handout%20-%20Letter.pdf) (linking to many more resources), [slides PDF](https://files.frameshiftconsulting.com/Ally%20Skills%20Workshop%20slides.pdf), [videos](https://www.youtube.com/watch?v=wob68Nl2440), and more.
- [Why cisgender allies should put pronouns on their name tag](https://medium.com/@mrsexsmith/dear-cis-people-who-put-your-pronouns-on-your-hello-my-name-is-nametags-78c047ed7af1)
