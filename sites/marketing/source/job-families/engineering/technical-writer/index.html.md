---
layout: job_family_page
title: "Technical Writer"
---

At GitLab, our [team of technical writers](https://about.gitlab.com/handbook/engineering/ux/technical-writing/) is responsible for ensuring that the [documentation](https://docs.gitlab.com/) for all of our products is clear, correct, comprehensive, and easy to use. We are looking for great writers with strong technical proficiencies to help our users succeed with our [rapidly evolving suite of DevOps tools](https://about.gitlab.com/releases/).

As part of our growing team, you’ll collaborate with GitLab engineers, who typically write the first draft of docs for the new features they create. You’ll dive in on special projects, planning and authoring new content, and helping to develop new doc site features and processes. You’ll collaborate with others across the organization to craft new and improved content. You’ll be at the leading edge of DevOps while contributing to one of the [world’s largest open-source projects](https://about.gitlab.com/blog/2017/07/06/gitlab-top-30-highest-velocity-open-source/) and engaging with our wider community.



## Job Grade

- The Technical Writer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

- The Senior Technical Writer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

- The Staff Technical Writer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Responsibilities

- **Continuously improve GitLab’s documentation content** in collaboration with engineers, product managers, and others.
   - Review and edit doc plans and content for all new and enhanced features.
   - Produce content for using GitLab features, including contextual content that clarifies why and when a user might use a feature or workflow.
   - Help developers and other members of the community who have documentation-related questions.
   - Identify and address content gaps or the need for additional media such as diagrams or videos.
   - Participate in reviews and revamps of section or page content and structure.
   - Help review and triage incoming suggestions, corrections, and other content from the GitLab community.
   - Help review and create UI text, such as field labels and error messages, in our product.

- **Continuously improve the GitLab documentation site features and user experience** in collaboration with engineers and other technical writers. These efforts might include the documentation site’s design, search, build process, feedback methods, SEO, visitor analytics, versioning, and other technical components.
   - Contribute to the planning and creation of new site features and enhancements.
   - Coordinate with frontend and backend engineers when their help is needed.

- **Contribute to the improvement of team process and style, as well as cross-functional efforts.**
   - Act as a reviewer of release blog posts and contributor to GitLab’s [Handbook](https://about.gitlab.com/handbook/).
   - Make it easier for contributors from the community, both internal and external, to submit high quality documentation.
   - Contribute to the documentation [Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html) and documentation [process guides](https://docs.gitlab.com/ee/development/documentation/).
   - Collaborate with and help improve upon collaborative processes with others including product managers, Support, Marketing, Engineering, and the wider GitLab community.

### Additional responsibilities for Senior Technical Writers

- **Documentation development**
   - Plan and complete large content initiatives, such as section reviews and revamps.
   - Through the use and testing of GitLab and its documentation, as well as SME interviews, proactively identify information needs and content opportunities. Develop new content to meet those needs and plan further work for the technical writing team.
   - Act as a primary reviewer for other technical writers.
- **Site features and user experience**
   - Plan and lead documentation site feature enhancement initiatives.
   - Work with UX to identify contextual content/documentation opportunities in the product.
   - Produce reports on documentation usage and metadata.
   - Contribute to the [Documentation Style Guide](https://docs.gitlab.com/ee/development/documentation/styleguide.html).
- **Leadership and collaboration**
   - Mentor newly hired technical writers.
   - Lead projects when pairing with other technical writers.
   - Plan new cross-functional efforts.
   - Represent the team at hackathons or other community endeavors.
   - Speak at conferences, meetups, or professional organization-sponsored events.
   - Plan and lead a portion of the team’s process-improvement initiatives.
   - Organize and prioritize portions of the team's backlog and other needs.

### Additional responsibilities for Staff Technical Writers

- **Documentation development**
   - Act as primary writer for larger, more complex content areas lacking a sole group/stage counterpart.
   - Provide guidance on content strategy for new stages and sections, and assist with content strategy on projects that span sections/stages.
   - Lead the development of key documentation strategies, including improving information architecture standards and product cohesiveness by helping to focus documentation on end-to-end use cases.
- **Documentation enablement and research**
   - Maintain the Technical Writing Style Guide, engaging others as needed.
   - Develop updated documentation processes, analyses, and standards that impact the work of the entire documentation team.
   - Regularly audit the GitLab blog and YouTube accounts for technical content that can be reused, and coordinate alignment between docs and Marketing content.
   - Analyze competitor documentation content and features, working with others at GitLab concerned with competitive intelligence.
   - Manage release post process improvements for Technical Writing.
   - Define docs metadata standards and manage implementation and maintenance, engaging others as needed.
- **Site features and user experience**
   - Plan and lead major documentation site feature enhancement initiatives.
   - Optimize and document the team’s use of docs toolchains and third-party services such as search and analytics products.
- **Leadership and collaboration**
   - Mentor other technical writers and lead documentation projects.
   - Act as a liaison with teams that have ongoing customer feedback on docs or other docs needs.
   - Act as a key contributor to team process and its documentation.

## Requirements

- You have:
   - Experience writing, editing, researching, and planning software documentation.
   - Excellent skills in grammar, minimalist documentation design, and effective information architecture.
   - Great teaching skills that translate into amazing written work.
   - Experience using the Linux shell, command-line Git, HTML/CSS, and/or at least one programming language (does not have to be in a professional context).
   - Experience using static site generators and managing docs as code.
   - Experience with some of the following:
      - Using or documenting DevOps tools
      - JavaScript and intermediate front-end development
      - Advanced programming or other technical experience
      - Using GitLab
      - A rapidly scaling start-up environment
      - Remote work; especially in collaboration with others across countries and time zones
- You are:
   - Highly organized; able to triage and prioritize numerous issues and projects.
   - Able to succeed in a remote, globally distributed work environment.
- You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.
- Experience with workflows and tooling used by engineering, operations, product teams.

### Additional requirements for Senior Technical Writers

- Extensive experience as a technical writer.
- Experience producing documentation for a variety of products and services.
- Some experience planning and leading major initiatives, such as the launch of new
documentation resources, large-scale documentation reviews and revamps,
and cross-functional initiatives.
- Experience with static site generators, managing docs as code, and the nuances
of differing markup standards and rendering engines.
- Experience establishing processes used by teams across an organization in support
of documentation.
- Demonstration of the use of data and evidenced-based decision making.
- Experience with DevOps tools and some degree of frontend or backend engineering.

### Additional requirements for Staff Technical Writers

- Extensive experience as a senior technical writer.
- Experience producing documentation for a large variety of products and services, including DevOps tools.
- Great research, planning, writing, editing, information architecture, and content strategy skills.
- Extensive experience planning and leading major initiatives, such as the launch of new documentation resources, large-scale documentation reviews and overhauls, and cross-functional initiatives.
- Experience conceiving and implementing processes used by teams across an organization in support of documentation.
- Extensive experience with DevOps tools and coding.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators

- [Technical Writing team member MR rate](/handbook/engineering/ux/performance-indicators/#technical-writing-team-member-mr-rate)
- [Distribution of Technical Writing team documentation effort](/handbook/engineering/ux/performance-indicators/#distribution-of-technical-writing-team-documentation-effort)

## Hiring Process

Candidates for this position can expect the hiring process to follow the process below. Please keep in mind that you can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their name or job title on our [team page](https://about.gitlab.com/company/team/).

* Candidates who demonstrate an interest in the role will be asked to share work samples and respond to 4 questions related to technical writing.
* Next, selected candidates will be invited to schedule a 30-minute [screening call](/handbook/hiring/interviewing/#conducting-a-screening-call) with one of our Global Recruiters. In this call, we will discuss your experience, understand what you are looking for in a Technical Writing role, talk about your work and approach to technical writing, discuss your compensation expectations and reasons why you want to join GitLab, and answer any questions you have.
* Next, if you successfully pass the screening call, you will be invited to schedule a 45-minute first interview with our Technical Writing Manager. In this interview, we will want you to talk through the approach you took in the examples of work you shared as part of your application, your experience up to now, understand what you're looking for in a technical writing position, and answer any questions you have.
* The next interview is with one of our Technical Writers. We’ll discuss your experience, why you’re looking to join GitLab, your domain experience, and what it's like to be a technical writer at GitLab. We’ll assess your alignment with our [values](/handbook/values/#credit), and answer any questions you have.
* Next, you’ll meet one of our Product Managers for a 45-minute interview. We’ll discuss your experience, why you’re looking to join GitLab, your domain experience, and what it's like to be a technical writer at GitLab. We’ll assess your alignment with our [values](/handbook/values/#credit), and answer any questions you have.
* Finally, you’ll meet our Senior Manager, Technical Writing. At this stage, we’ll look to understand your views on how documentation impacts user experience, your experience up to now, and answer any questions you have.
