---
layout: handbook-page-toc
title: "Professional Services Instructional Design and Development"
---


# What's New?

## [GitLab Technical Certifications](/handbook/customer-success/professional-services-engineering/gitlab-technical-certifications/)

### GitLab Certified Associate

- Soft-launched in: May 2020
- Description page: [GitLab Certified Associate](https://about.gitlab.com/services/education/gitlab-certified-associate/)
- Related course: [GitLab with Git Basics](https://about.gitlab.com/services/education/gitlab-basics/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

### GitLab CI/CD Specialist

- Soft-launched in: July 2020
- Description page: [GitLab CI/CD Specialist](https://about.gitlab.com/services/education/gitlab-cicd-specialist/)
- Related course: [GitLab CI/CD Training](https://about.gitlab.com/services/education/gitlab-ci/)
- Also available at: [GitLab Commit 2020](https://about.gitlab.com/events/commit/)

### GitLab InnerSourcing Specialist

- Soft-launched in: June 2020
- Description page: [GitLab InnerSourcing Specialist](https://about.gitlab.com/services/education/gitlab-innersourcing-specialist/)
- Related course: [GitLab InnerSourcing Training](https://about.gitlab.com/services/education/innersourcing-course/)

#### GitLab Certified Project Management Specialist

- Soft-launched in: December 2020
- Description page: [GitLab Certified Project Management Specialist](https://about.gitlab.com/services/education/gitlab-project-management-specialist/)
- Related course: [GitLab for Project Managers Training](https://about.gitlab.com/services/education/pm/)

#### GitLab Certified Security Specialist

- Soft-launched in: December 2020
- Description page: [GitLab Certified Security Specialist](https://about.gitlab.com/services/education/gitlab-security-specialist/)
- Related course: [GitLab Security Essentials Training](https://about.gitlab.com/services/education/security-essentials/)

#### GitLab Certified DevOps Professional

- Soft-launched in: December 2020
- Description page: [GitLab Certified DevOps Professional](https://about.gitlab.com/services/education/gitlab-certified-devops-pro/)
- Related course: [GitLab DevOps Fundamentals Training](https://about.gitlab.com/services/education/devops-fundamentals/)

## GitLab Professional Services Game Library

The microlearning game library is designed to address topics of interest in order to increase memory retention and understanding during the learning experience. If you wish to see a preview of these games, email Proserv-education@gitlab.com.

Available Learning Games:

- Battleship Challenge
- Super Mario Adventure
- GitLab Treasure Hunt

# What's in Progress?

If you want to see what is currently being developed or planned out, check out the [Education Services Activity Board](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/activity)

- GitLab System Administration Specialist
- GitLab System Administration Professional 
- GitLab CI/CD Professional 
- Self-Paced GitLab CI/CD 


### Workflow Labels

Here are the main labels used for PS instructional development projects.

`ProServ-practice::Education`

`Workflow::scheduling` - ID begins the storyboarding phase and begins planning out the course development.

`Workflow::ready for development` - ID determines project is ready for development work to begin and builds issues and epics for work tracking. 

`Workflow::in dev` - The individual components for the Education Services solution are being designed and developed. 

`Workflow::ready for review` - The ID distributes the drafts to the SMEs and approved review team for approvals.

`Workflow::in review` - The ID completes any necessary edits and/or additions that were determined necessary during the 'ready for review' stage.

`Workflow::blocked` - not used for ID purposes.

`Workflow::verification` - This represents the pilot delivery phase for an educational offering

`Workflow::staging` - The ID begins the handbook additions and/or wiki instructions for any course changes/additions prior to implementation.  

`Workflow::production` - The Education Services solution is live and can be accessed by the target audience

### Have an Idea for a New Training?
To submit an Education Request, create a new issue and select the [education-request template](https://gitlab.com/gitlab-com/customer-success/professional-services-group/education-services/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) and fill out the necessary details.
