---
layout: handbook-page-toc
title: Solutions Architect Processes
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

# Solutions Architect Processes
{:.no_toc}

## SA Activity Capture

Solutions Architects record all customer and prospect activity to promote **transparency** and **collaboration**. By consistently capturing our customer facing activity, we can perform analysis for further reporting to establish **efficient** decision making for GitLab's business. More information on this process can be found on the [SA Activity Capture](/handbook/customer-success/solutions-architects/processes/activity-capture) page.

## Engaging Professional Services

Follow the process detailed in the [Working with Professional Services
](handbook/customer-success/professional-services-engineering/working-with/#for-sales-reps-and-sas-how-to-order-professional-services) handbook page.

Simplified process description:
- Ensure that PS Opportunity has already been created by SAL / AE.
- If it a standard (non-customized) service from our [full catalog](https://about.gitlab.com/services/catalog/).
   - SAL / AE to order PS directly from Zuora in SFDC.
- If standard services do not meet the needs of the customer
   - Use the [Services Calculator](http://services-calculator.gitlab.io/) to generate an issue and a draft quote.
   - Iterate on that issue with PS and SAL / AE.

## Customer Security Assurance

Follow the process detailed in the [GitLab's Customer Assurance Activities
](/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html#customer-assurance-activities-workflow) handbook page.

Simplified process description:
- Start with [Security - GitLab Trust Center](https://about.gitlab.com/security/).
- Encourage customers to use and review [Self-service Information Gathering](handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html#customer-self-service-information-gathering).
- SAs are expected to attempt a first-pass for all security questionnaires
   - Do it, it’s fun and educational!
   - Make use of the [GitLab AnswerBase](handbook/engineering/security/security-assurance/risk-field-security/common-security-questions.html) (200+ pre-answered questions)
- Additional requests can be made in [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70) Slack Channel.

## Generating Licenses

- SAs and SALs no longer have access to the LicenseDot tool.
- In order to support prospects and customers, refer to the [Internal Support](handbook/support/internal-support/#regarding-licensing-and-subscriptions) processes handbook page.

## Cross-functional Sales Team Processes

### Working Agreements

Enterprise Solutions Architects typically support two sales teams made up of Sales Development Representatives, Strategic Account Leaders and Technical Account Managers. Commercial Sales Solutions Architects support Mid-Market Account Executives and SMB Customer Advocates in a pooled model. When joining a sales team, establishing working agreements is critical to providing optimal service to the customers as well as the GitLab team. A sample template of working agreements is found below to help facilitate conversation and establish these agreements:

1. **Customer response time for emails and meeting followups** I will always do my best to provide same-day responses to customer inquiries and follow ups unless otherwise noted. I like to provide customers top-notch service, but interruptions can affect that target. I will use my out of office when traveling so customers can expect delayed responses during those times. Feel free to contact me if it's approaching the end of the day and you didn't see me address a customer request. Slack is the easiest way to find me most of the time.
1. **Calendar** Schedule things as necessary right on my calendar during working hours - I'll do my best to keep my calendar up to date. However, if you see only one empty slot for a day or if it looks like the meeting would be back to back with something else, please check with me before confirming dates and times with the customer. If you want me at my best for you, I will need time to prepare and followup. If we schedule back to back, I've found I'm often late to arrive due to the last meeting, flustered as I context shift, lacking answers for known questions, unprepared with demo environments that were altered in the last call, etc.
1. **Regular communication** A weekly strategy hour-long call with the SA, SAL, SDR and TAM is preferred. If more frequent synchronous communication is desirable or required, we should work together as a team to identify a viable cadence, required attendees, convenient time and agenda for those calls.
1. **Elevated conversations** The more we focus on value and stay away from very deep technical and troubleshooting conversations, the more I can help you achieve target sales. I'm happy to review and contribute to business plans so we remain in sync.
1. **Professional Services** I'm happy to recommend and propose services for prospects and customers. I will write the initial SOW and get approvals from the Professional Services team.
1. **Travel** While strategic enterprise sales absolutely require travel, each day on the road can increase the volume of work and calls on other days of the week as my dialogs with customers often require extensive time commitments to research and model specific customer solutions. Please plan strategically when looking at on-sites, considering that I support at least two sales teams. Additionally, please do what you can to avoid scheduling on-site visits less than one week out to help me be able to commit to family and local things.
1. **Notes** Unless otherwise directed, call notes will be linked to a Google document from Salesforce. Email communication with customers will also be bcc'd to Salesforce. If you prefer to record notes and actions differently, please let me know what that collaborative method is.
1. **Continuous learning** Things at GitLab move really fast, and I need time to keep up. I'll block a few hours on my calendar coincident with releases to absorb the latest GitLab feature set, but new technologies and competitive products also appear regularly. Please expect that I will need to allocate additional time to learning about those on a regular basis to sufficiently support our customers.

### Tool Usage within Cross-functional Teams

1. **Salesforce**
[To-do] Add info on views and reports to use during the sales cycle. Also, guidance on SA being listed on opportunities and accounts (there is not any info in our current handbook)
1. **Gainsight** Technical Account Managers user Gainsight to manage the ongoing customer lifecycle. Others in sales have view access to the Gainsight data. The easiest way to access Gainsight is via Salesforce. See the TAM handbook section [Using Gainsight with Customer Success](https://about.gitlab.com/handbook/customer-success/tam/gainsight/) for details.
1. **Slack** Create a public internal team slack channel for your cross-functional team. This will allow you to collaborate easily without sending DM's.
1. **Google Drive** There is a shared [GitLab Sales](https://drive.google.com/drive/folders/0BzQII5CcGHkKSFFJWkx3R1lUdGM?usp=sharing) folder. Running customer notes and other documents related to a specific customer should be stored in the [Customers and Prospects Subfolder](https://drive.google.com/drive/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U?usp=sharing) under the appropriate letter and customer name subfolder.
1. <b>[GitLab Account Management Project](https://gitlab.com/gitlab-com/account-management)</b> may be used for a POV and/or TAM collaboration with a customer post-sale.
1. <b>[Chorus.ai](https://chorus.ai/)</b> is used to record meetings. The GitLab Notetaker will automatically join meetings with people outside of GitLab once your account is set up.

### Quarterly Business Reviews (QBR)

The entire cross-functional team should help in the preparation of the QBR content that the sales rep will present during the QBR, and should participate in the QBR session. It is often helpful to perform a dry run of the QBR with the area sales manager prior to the actual meeting, to review the content and timing of the presentation.

### Engaging an SA During the Sales Cycle

Solution Architects may participate in initial qualifying meetings (IQM's) or allow the SDR and SAL/AE to manage the initial call and utilize information gained during it for additional preparation. Teams can handle this on a case by case basis.

Solution Architects should participate in [technical discovery](#technical-discovery-and-demo-preparation) after lead qualification is complete and in other activities during the sales process that lead to a technical win, e.g.

- GitLab demos
- GitLab technical deep dives
- Whiteboarding sessions
- Prospect Q&A sessions
- RFI/RFP completion
- [Security Audits](https://about.gitlab.com/handbook/engineering/security/security-assurance/risk-field-security/customer-security-assessment-process.html)
- [SOW Scoping with Professional Services] (https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/)

SA's may also work in tandem with a TAM to support existing customers, especially when expand opportunities exist within the account. And SA's may also have regular touch points smaller customers who do not have a TAM assigned.

#### [SA/TAM Engagement Overlap](https://about.gitlab.com/handbook/customer-success/#overlap-between-solution-architects-and-technical-account-managers)

#### Technical Discovery and Demo Preparation

Prior to any technical discovery and demo preparation, the SA should be provided with the following information about the current opportunity:

- Share any existing notes about the opportunity
- Key players and personas
- Other relevant information shaping an engagement
- Is there a business plan or sales approach defined?
- What next steps are we hoping to drive from the next call?

The Solutions Architect, in order to tailor conversations and demos to demonstrate value as well as address current problem areas, will want to understand the following information, and may request further technical discovery prior to any demo:

1. **Outcome:**

- What’s in it for the client?
- Why look at a new strategy for software development?

1. **Infrastructure:**

- What tools are currently in place?
- What tools have potential to be replaced?
- What processes or workflows have potential to change?

1. **Challenges:**

- What problems/roadblocks have been uncovered?
- What is the current release velocity?
- What is the current planning process?
- What visibility, traceability or efficiency concerns exist?
- What collaboration or governance opportunities exist?
- What security or compliance inefficiencies exist?

## Solution Architect Engagement Models

### U.S. Strategic Account Engagement Model

U.S. Enterprise SA's are aligned regionally with Strategic Account Leaders and aligned in their respective account activities. Each team collaborates according to their own Working Agreements.

When workload exceeds the SA's capacity or when there is a request from other departments, please submit the request for assistance on either of the enterprise triage boards based on region: [US EAST Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/us-east-enterprise-triage/boards) or [US WEST Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/us-west-enterprise-triage/boards). The issue creation and triage processes are described in detail below.

### EMEA Account Engagement Model

SA engagement for customer interactions, RFP's, audits and more can be requested by any EMEA-based sales executive or other GitLab team-member using the appropriate GitLab issue board. The [EMEA Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-triage/boards) is leveraged by the team in order to ensure coverage for all sales activity. The issue creation and triage processes are described in detail below.

### Commercial Engagement Model

SA engagement for customer interactions, RFP's, audits and more ([how to engage a Commercial SA](/handbook/customer-success/solutions-architects/processes/commercial/)) can be requested by an SMB or Mid-Market Account Executive or other GitLab team-member using the appropriate GitLab issue board. Commercial boards exist for both the Americas and EMEA. The [Americas Commercial Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966) and the [EMEA Commercial Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/emea-commercial-triage/-/boards) are leveraged by the team in order to ensure coverage for all sales activity. The issue creation and triage processes are described in detail below.

### APAC Account Engagement Model

Customer Success Team (SA/TAM) engagement for customer interactions, RFP's, audits and more can be requested by a Strategic Account Leaders, an SMB or Mid-Market Account Executive or other GitLab team-member using the appropriate GitLab issue board. A single APAC Triage board exists that serves both Enterprise and Commerical markets. The [APAC Triage Board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/apac-triage/-/boards/1354580) is leveraged by the team in order to ensure coverage for all sales activity. The issue creation and triage processes are described in detail below.

### Issue Creation Details

1. Navigate to the correct board for your region. NOTE: All triage boards are located in the [SA Triage Boards](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards) group in projects broken out by region or engagement model.
1. Create a new issue.
1. Use one of the available templates:

- New activity: use the "SA Activity" template to ensure the proper data is collected and available
- Follow up activity: use the "Follow Up" template
- Security audit: use the "Vendor Security Assessment" template

1. To help the SA group ensure success for the client, the following information should be made available _prior_ to SA engagement

- **Customer Information**: Customer name, SFDC opportunitylink
- **Preferred Date Options**: When would the customer want to have the call (including time zone information)
- **Target Hosting Environment**: Self-hosted, cloud or GitLab.com?
- **Challenges/Pain Points**: What is the customer pain/problems?
- **Required Capabilities**: What does the customer need?
- **Current Tools/Competitors**: What is the current tool set? Are any of those tools non-negotiable?
- **SA Asks**: What do you need from the SA's (demo, technical Q&A, SOW, etc.)

1. Add one or more of the following labels as needed (labels listed below).

### Board Label Descriptions

- **Open/no label**: Newly submitted issues
- **SA Triaged**: Issue ownership has been claimed by an SA
- **SA Demo Ready**: Prep has been completed for a customer interaction and further scheduling or conversations can now occur
- **Security Audit**: This request requires security team involvement, typically upon receiving a customer security questionnaire
- **Services Request**: If this opportunity focuses on services, use this label to indicate that
- **SA Doing**: Once an SA signals they are actively working the issue, the issue is moved to 'Doing'. The issue will stay in this status until the SA, customer and sales team agree the issue has been completed
- **SA Followup**: Followup work is required by the SA for this particular request (example: questions needing research after a call ends)
- **SA Waiting**: This label indicates that an SA is waiting on the requestor for more information before advancing this issue
- **SA POV**: A Proof of Value is being requested or needs support

### Triage of Issues

The SA team associated with each region will monitor the triage board and/or an associated Slack channel for incoming work and will contact the appropriate Customer Success team members via Slack, email or phone depending on required skills, schedules and availability. SLA for activity is 48 hours unless the issue is marked with an 'Expedite' label.

## SA Participation in Marketing Events

When technical staff is required for a marketing event, follow the process outlined on the [Marketing Events](/handbook/marketing/events/#requesting-technical-staffing) page. SA managers and others in Customer Success should monitor the [Event Staffing Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1254947) regularly for new events (although Marketing staff will CC the appropriate managers on issues.)

## Request Solutions Support from Product

When a prospect or customer have required capabilities that do not map directly to our product offering, Solutions Architect’s first step is to explore solutions or work-arounds that would satisfy their requirements with our current toolset.
If the SA is unable to determine an approach that works for the prospect, the next best action is to reach out to the Product Manager. To determine what Product Manager is appropriate, review the [Product Categories handbook page](/handbook/product/categories/).

To be efficient as possible, this is the recommended approach:

1. **Gather**: Document and articulate the Customer journey
    1. Start by creating an issue on their [Account Management Project](https://gitlab.com/gitlab-com/account-management). Include the customer business objectives, existing approach, and communication between the prospect (call recordings, emails, etc).
    1. Reference running logs in the [Customers & Prospects](https://drive.google.com/drive/u/1/folders/0B-ytP5bMib9Ta25aSi13Q25GY1U) folder
1. **Explore**: If applicable, document the solutions or work-arounds that were explored with the customer and why that was not sufficient
1. **Engage**: Reach out to the Product Manager
    1. Create a new slack channel with SA/SAL/PM/etc with a link to the Account Management issue
    1. Be mindful and gift a minimum of 48-96 hr lead time.  _Properly done, PMs should have at least a weeks notice_
    1. Collaborate using the Account Management issue, history, running docs, and shared material
    1. [optional] Suggest an internal team call to coordinate materials and next steps
1. **Communicate**: Set up a call with the prospect or customer to determine the next best steps.
