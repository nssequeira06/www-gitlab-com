---
layout: handbook-page-toc
title: "Rules of Engagement for Selling Professional Services"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Rules of Engagement (ROE) for Selling GitLab or Partner Professional Services


# Objective

Define when and how a GitLab seller engages with GitLab PS or a GitLab partner for delivering professional services that support customer licensing opportunities and customer success.


# Important to services note

Please remember that as you evaluate your customer/prospect’s needs and build an understanding of their requirements a key success factor will be if the customer/prospect needs services.  The earlier identify and address your customer/prospect/s needs and requirements for services the quicker your licensing opportunity will close.  


# Key Takeaways



1. If the customer already has a services partner that they or you are working with for the customer’s DevOps/GitLab solutions, then start by engaging with that partner to see if they can deliver against the customer's needs.
2. For Large & Mid-Market customers, if the answer to 1 is “there is no partner,” then GitLab Professional Services has first right of refusal for professional services needs & outcomes.  Follow the Professional Services process and GitLab Professional Services will support the customer’s needs or they will tell you to identify and engage a partner.
3. ALL SMB needs will leverage Channel service provider partners.


# Rules of Engagement


<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTj6tGFScRjVYDk3tj1t5CpRn1bBI12fsUm1IttZ0Pl6AO1_3-DU5I2Gmic9QR_yZvT5SFrKEGLwK4f/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

<br>

# GitLab Professional Services Process

Here is the [handbook page for the steps in selling GitLab Professional Services](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/).


## Timing and Expectations

For Large and Mid-Market needs, when you engage GitLab Professional Services through the process, they will schedule an opportunity review with you within 2 business days of the issue being added to PS Engagement Management team’s queue during Step 2 (Create the opportunity in SFDC) of the process outlined in the [handbook](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/selling/).


# Partner Identification & Engagement Process

For SMB needs as well as any opportunities that GitLab Professional Services recommends for partner engagement here is the process to follow:


## Step 1: If you know a qualified partner

If you have experience with a fitting/qualified GitLab partner, contact the partner directly.  When briefing or following up with the partner, please CC the partner’s Channel Account Manager (CAM info can be found in SFDC in the Account record for the partner) so that they can support your efforts.


## Step 2: Identifying partners by Segment and Region in #channel-sales

When identifying a partner you will leverage the #channel-sales slack channel by @mentioning various people or teams as follows:


### SMB

Americas: @ mention Jordan Bushong in #channel-sales

EMEA: @ mention Bastian van der Stel in #channel-sales

APAC: @ mention Amelia Seow in #channel-sales


### Mid-Market

Americas: @ mention Jordan Bushong in #channel-sales

EMEA: @ mention Bastian van der Stel in #channel-sales

APAC: @ mention Amelia Seow in #channel-sales


### Large

General note on Global System Integrators (GSI): if the customer feels like a candidate for a GSI engagement, @ mention Alan Geary in #channel-sales

Americas: If you know the CAM aligned with your state/country @ mention them in #channel-sales, if you don’t then @ mention Alan Geary, Sergio Cortes and Jen Bailey in #channel-sales

EMEA: @ mention Matthew Coughlan and Kevin Franklin in #channel-sales

APAC: @ mention Amelia Seow in #channel-sales


## Timing and Expectations

The Channel Teams will acknowledge your request within the same business day and will provide a warm/vetted intro for you within 1 business day.  
