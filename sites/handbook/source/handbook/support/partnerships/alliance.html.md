---
layout: handbook-page-toc
title: Alliance Partners
description: "Support specific information for alliance partners"
---

Also known as Technology Partners, Alliance Partners are looking to integrate
with our solution. Through product integrations, GitLab helps developers
compile all their work into one tool that can be accessed anywhere. We work
closely through partnerships to provide developers with a single DevOps
experience.

## Contacting Support

Alliance Partners contact us via the
[support portal](https://support.gitlab.com). To ensure that their tickets
submitted there route properly, they use
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001172559).

This opens the ticket in the Alliance Partner form in zendesk.

**Note**: Pay close attention to any organization notes the Alliance Partner
might have on their account. These often provide critical information about how
to provide the very best support possible!

## File uploads

When Alliance Partners need to send files to GitLab Support, we have 3
methods available to them:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)
* A specialized connection to an s3 bucket
  * For this method, Support will use an application that is already
    available to them for listing and downloading the files as needed.

## Escalations

Alliance Partners work closely with GitLab Support. To accommodate this, GitLab
Support provides all Alliance Partners with a personalized escalation form. When this
form is submitted, the system notifies an appropriate GitLab Support Manager
to help escalate a ticket the Alliance Partners have opened.
