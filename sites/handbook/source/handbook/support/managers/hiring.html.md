---
layout: handbook-page-toc
title: Support Hiring
description: "Hiring new Customer Support team members is a multi-department process. An outline of the responsiblities of Customer Support in that process."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Hiring new Customer Support team members is a multi-department process involving recruiting and customer support. 

Support Engineers comprise the vast majority of Customer Support. This page presents information specific to that job family. Exceptions are called out explicitly, such as in the [Reviewing Assessments](#reviewing-assessments) section.

## Hiring flow

Hiring is described on the [jobs page](/job-families/engineering/support-engineer/#hiring-process) for Support Engineers. The following are the stages carried out by Customer Support team members.

### Technical questionnaire

**GitLab Participant**: Support Engineer

The assessment questionnaire contains a number of customer scenarios and technical questions relevant to the position, including written
English ability.  Support engineers are responsible for reviewing the candidate responses.

### Technical interview

**GitLab Participant**: Support Engineer

The technical interview for this role is conducted by support engineers. It is a practical interview that covers topical areas relevant to the position, including customer scenarios.

### Behavioural panel interview

**GitLab Participants**: Support Manager and one other (Support Manager or Support Engineer).  At least one interviewer will be non-male.

The behavioural interview may cover any topics of the previous interviews where there was unclear data or a need to dive deeper.  

###  Final interview

**GitLab Participants**: Senior Support Manager or Director, Customer Support

Candidates successful in all previous interviews may proceed to a _Senior Manager_ or _Director, Customer Support_ interview. 

## Getting involved
If you want to be involved in the hiring process - let your manager know! The main ways you can be involved are:
- reviewing technical assessments
- conducting technical interviews

After getting approval from your manager open two interview training issues:

- [PeopleOps Interview Training](https://gitlab.com/gitlab-com/people-ops/Training/issues/new?issuable_template=interview_training)
- [Support Interview Training](https://gitlab.com/gitlab-com/support/support-training/issues/new?issuable_template=Support%20Interview%20Training)

### Reviewing Assessments
Assessments (and answers) for all Support job families are available in the [people-ops/hiring-processes](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Support) repository.

When reviewing an assessment: 
1. Use the rubrik for the assessment that was sent along to determine if they passed or not.
2. Review their CV, Cover Letter and any attached documentation. Does this person possess the background and skills to succeed in your role?
3. Fill out the scorecard with any comments. Keep in mind that all fields are optional with the exception of the "Overall Recommendation". 
If you're unable to offer an evaluation of an area you may leave it blank. 

### Technical Interviews
The technical interview process is documented in the [Support Team project](https://gitlab.com/gitlab-com/support/tech-interview/se-interview). Follow the instructions there to conduct this interview. Take any notes in Greenhouse and make a note if you are inclined or not inclined to hire. Your role in the interview process for this candidate is complete!

## Pre-Hiring
Before you can do any of the above you'll need to have an open requisition and a vacancy in that requisition. You can learn more about these terms and the process at [Requisitions and Vacancies](/handbook/hiring/vacancies/) in the [Hiring Section of the Handbook](/handbook/hiring/).

### How a candidate gets routed to a manager

#### Hiring Plan
The VP of Customer Support is the DRI for the hiring plan. They will ensure that headcount is allocated to regions and approximate hiring dates.

#### Vacancies
1. When headcount is allocated, the [Support Hiring Report - Hiring Plan](https://drive.google.com/drive/u/0/search?q=title:%22support%20hiring%20reports%22)(internal only) will be updated by the hiring team.

For each vacancy there should be a:
- **Job title**
- **Region**
- **Hiring Manager**
- **Opening ID**
- **Opening Status**
- **Target/Actual Start Date**
- **Candidate**

1. When we get close to the target start date, the recruiting team will update Greenhouse with the vacancy and assign it an **Opening ID**.
1. When a vacancy is open in Greenhouse, the hiring manager (or Sr. Manager) will be noted in the spreadsheet.
