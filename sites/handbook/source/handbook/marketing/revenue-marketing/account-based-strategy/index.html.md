---
layout: handbook-page-toc
title: "Account Based Strategy"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Account Based Strategy
The account based strategy team sits in [Revenue Marketing](/handbook/marketing/revenue-marketing/) and is responsible for the following:
- Developing, in partnership with sales and marketing, and managing our [ideal customer profile (ICP)](/handbook/marketing/revenue-marketing/account-based-strategy/#ideal-customer-profile)
- [Account based marketing (ABM)](/handbook/marketing/revenue-marketing/account-based-strategy/#account-based-marketing-abm) campaigns and tactics
- [Demandbase](/handbook/marketing/revenue-marketing/account-based-strategy/#demandbase)
    - platform management
    - account based engagement
    - campaign orchestration
- Maintenance of GitLab's [focus account lists](/handbook/marketing/revenue-marketing/account-based-strategy/#focus-account-lists)

## Account Based Marketing (ABM)
Account-based marketing is a focused B2B marketing approach in which marketing and sales teams work together to target key or "best-fit" accounts. These accounts are marketed to directly, as a market of one (compared to the typical one-to-many approach).  At GitLab, we focus on the top net new and growth accounts globally.  For more information, head over to our [ABM handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/account-based-marketing/)

## Demandbase
Demandbase is a complete end-to-end solution for Account-Based Marketing. We primarily use Demandbase as a targeting and personalization platform to target online ads to companies that fit our ICP and tiered account criteria. Demandbase also has a wealth of intent data that is available to us through its integration with Salesforce.  Check out the [Demandbase handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/demandbase/) for more information.

## Focus account lists
Focus list of accounts that are [first order logos](/handbook/sales/#first-order-customers) (first paid customer within an organziation) for GitLab and fit our ideal customer profile for both Large (GL4300) and MidMarket (MM4000). The lists are developed using the current data sources we have at our disposal and are refreshed quarterly at the beginning of each quarter. For more information, please head over to our [Focus account list handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/focus-account-lists).

## Ideal Customer Profile
An ideal customer profile is the description of our "perfect" customer company (not individual or end user).  The profile takes into consideration firmographic, environmental and additional factors to develop our focus list of highest value accounts. Visit the [ICP handbook page](/handbook/marketing/revenue-marketing/account-based-strategy/ideal-customer-profile/) for more information.

### Project and priority management
The account based strategy teams maintains several board to help manage our projects, priorities and workflow.  The main board is the [Account Based Strategy](https://gitlab.com/groups/gitlab-com/-/boards/2297659) board and has the following labels:

- Account Based Strategy- anything the team is working on that crosses over multiple projects i.e. working with Sales Ops on account data updates
- Account Based Marketing- specific to our accoutn based marketing strategy and campaigns
- Ideal Customer Profile- covers cross functional work on maintaing our ICP
- Focus Account Lists- for updates and management of our focus account lists, as well as campaigns targeting these lists
- Demandbase- anything related to Demandbase platform management, intent data, and orchestration (DB campaign requests should be open in the project that covers the audience the campaign is launching to)
