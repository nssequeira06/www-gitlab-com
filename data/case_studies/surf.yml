title: SURF
cover_image: '/images/blogimages/surf.jpg'
cover_title: |
  How SURF increased deployment speed by 1400% 
cover_description: |
  SURF adopted GitLab for version control, but also found success with CI/CD, IaC, and SSoT.
twitter_image: '/images/blogimages/surf.jpg'

twitter_text: "Learn how @SKA_telescope is building the world's largest telescope with GitLab."

customer_logo: '/images/case_study_logos/surf.svg'
customer_logo_css_class: brand-logo-tall
customer_industry: Science and Research
customer_location: Amsterdam
customer_solution: GitLab Premium
customer_employees: 300 
customer_overview: |
  SURF was looking for a single solution for version control, but GitLab features provided much more. 
customer_challenge: |
  SURF was using several platforms in various ways and was looking for a single solution to enhance version control.

key_benefits:
  - |
    Improved version control 
  - |
    Faster deployment time
  - |
    Single source of truth
  - |
    Kubernetes integration
  - |
    Leveraged IaC
  - |
    Enhanced collaboration 

customer_stats:
  - stat: 1   
    label: Day deploys down from 2 weeks
  - stat: 25      
    label: Minutes to create a product environment cluster 
  - stat: 20      
    label: MRs a day up from 10

customer_study_content:
  - title: the customer
    subtitle: IT supporting science research
    content:
      - |
        Formed in 1974, SURF is a cooperative association that supports the national scientific and educational communities and institutions in the Netherlands and their worldwide partners like CERN. Within the field of research, SURF provides IT solutions, platforms, and architectures for data management, analytics, and computation to its members.
      - |
        [SURF](https://userinfo.surfsara.nl/) supports over one hundred institutions between medical research centers, universities, research institutes and space exploration centers across the nation. Driving innovation together. That is the mission of SURF. Together with the institutions, we ensure that the education and research community has access to the finest and newest ICT facilities for top research and talent development.
      
  - title: the challenge
    subtitle: Lacking version control and collaboration
    content:
      - |
        SURF was previously using several tools in a very scattered environment. Each team was using different platforms in various ways, including SBC, GitHub, and GitLab CE. With so many different installations on premise per group, developers were disconnected from one another and lacked the ability to truly collaborate.   
      - |
        SURF needed a solution that could act as the single source of truth for the entire organization. Teams needed one solution for user management and granular repository access. The goal was to provide an improvement of processes, an overall organization of projects, and enhanced user management capabilities.
    
  - blockquote: Because of CI/CD, development and operations teams have a framework of co-operation. This is probably the highest value Gitlab has introduced in our teams.
    attribution: Giuseppe Gianquitto  
    attribution_title: Cloud Lead Architect, Research and Development Services

  - title: the solution
    subtitle: More than a version control system
    content:
      - |
        SURF was previously using GitLab primarily as a Git tool. As they researched tools for version control, they discovered that GitLab Premium offers versatility and user management features that other solutions don’t provide. 
      - |
        SURF adopted GitLab Premium as a versioning platform to run on premise. Teams started by using GitLab for version control and then formed a step-by-step process for [CI adoption](/topics/ci-cd/implement-continuous-integration/).

  - title: the results
    subtitle: Increased deployment, code management, and IaC
    content:
      - |
        Deployment times have decreased tremendously, allowing SURF to deliver solutions to research and education communities much faster. “We are coming from a place where you buy the hardware, you rock it, you plug it, you install the operating system. From this, we moved directly to deployment using GitLab CI for infrastructure as a code. Deploys went from two weeks to one day. So it took two weeks to deploy a cluster. Now it can take one day if it's a really, really complex case. That's the worst case scenario,” said Giuseppe Gianquitto, cloud lead architect, Research and Development Services. 
      - |
        Workflow is simplified not only because of the [GitLab Flow](/topics/version-control/what-is-git-workflow/), but also because of CI/CD templates; which empowers teams to leverage Infrastructure as Code (IaC) and deploy complex systems in a streamlined and repeatable fashion. “Gitlab is the backbone of our deployments, from data analytics to IoT distributed platforms, we rely on Gitlab to rapidly create complex solutions,” said Machiel Jansen, head of Scalable Data Analytics group.    
      - |
        The Research and Development Services unit within SURF designs and deploys Kubernetes clusters as IaC with applications spread across dozens of repositories using a modular and pluggable design with Gitlab as the backbone. “We deploy Kubernetes clusters for our developers. So those clusters, they come up with everything ready for the developer to create an application, production-ready for the final user. Now, the entire process for us to the product cluster is less than 25 minutes,” said Gianquitto. “From the moment that the developer says, ‘I need a new cluster, because I need to go into production with the municipality of Amsterdam or with a project for CERN,’ the environment is ready within 25 minutes.” 
      - |
        GitLab runners are responsible for authenticating to the cloud environment, and typically run Terraform and Ansible deployments to set up the required infrastructures and applications. Over 20 Kubernetes clusters and over 300 nodes are serving Universities and Institutes with complex workloads.  
      - |
        The microservices running on those Kubernetes clusters, from internal platforms to the end services, are designed across Gitlab repositories. Creating, scaling, and deleting those applications and infrastructures is entirely driven by Gitlab CI. “Our DevOps teams can provision production-ready solutions within minutes, from Kubernetes to multi-cloud and multi-tier complex hybrid cloud infrastructures. Gitlab CI/CD in synergy with Terraform gives us all we need to provide our researchers and data scientists with reliable and re-usable tools,” Gianquitto said.
      - |
        SURF now has a single platform for Git, version control, code management, IaC, and CI/CD. Developer throughput has increased and cross-functional relationships have improved. “Mostly because CI/CD, development and operations teams have a framework of cooperation. This is probably the highest value Gitlab has introduced in our teams,” Gianquitto said.
      - |
    
      
      - |
        ## Learn more about Premium
      - |
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
      - |
        [The benefits of GitLab CI](/stages-devops-lifecycle/continuous-integration/)
      - |
        [Choose a plan that suits your needs](/pricing/)
        
